# assignment 4 P



# Terraform README

This README for detailed explanation of steps and result ss 




## steps followed 


1. Clone this repository to your local machine.
2. Navigate to the appropriate directory containing the Terraform code.
3. Initialize the Terraform working directory by running `terraform init`.
4. Review the execution plan by running `terraform plan`.
5. Apply the changes by running `terraform apply`.
6. Meanwhile paste the init.pp file under the manifests directory of the puppet server.
7. After step 5 gets completed, sign the puppet agent certificate from the puppet master instance.




## result-Screenshots

![Screenshot](ss\screenshot2.png)
![Screensot2](ss\terraform_screenshot.png)